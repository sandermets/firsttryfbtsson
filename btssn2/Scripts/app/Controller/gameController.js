﻿app.controller("gameController", function ($scope, $http) {

    $scope.btnSartText = "Start!";
    $scope.btnDealText = "Deal!";
    $scope.btnBreakText = "Break!";
    $scope.btnStart = false;
    $scope.btnDeal = $scope.btnBreak = true;

    $scope.money = 100;
    var breakNotActive = true;

    var _refreshScore = function () {

        var ix = 1,
            acesCount = 0,
            playerWon = false;

        $scope.currentPoints[0] = 0;

        while (ix < 6) {//reset ACE related combinations

            if (ix in $scope.currentPoints) {
                $scope.currentPoints.splice(ix, 1);
            }
            ix++;

        }
        
        angular.forEach($scope.selected, function (v, k) {
            acesCount += v.sign == 'A' ? 1 : 0;
            $scope.currentPoints[0] += v.points[0];
        }, acesCount);

        //if min points are over 21 the game is lost
        if ($scope.currentPoints[0] > 21) {
            $scope.gameMessage = 'Better luck next time!';
            $scope.btnDeal = $scope.btnBreak = true;
            $scope.btnStart = false;
        }

        if (acesCount == 1) {
            //woA - score without aces min points
            var woA = $scope.currentPoints[0] - 1;
            $scope.currentPoints[1] = woA + 11;

        } else if (acesCount == 2) {

            var woA = $scope.currentPoints[0] - 2;
            var r = woA + 1 + 11;
            if (r < 22) {
                $scope.currentPoints[1] = r;
            }
         
            //$scope.currentPoints[1] = woA + 1 + 1;
            //$scope.currentPoints[1] = woA + 1 + 11;

        } else if (acesCount == 3) {

            var woA = $scope.currentPoints[0] - 3;
            var r = woA + 1 + 1 + 11;
            if (r < 22) {
                $scope.currentPoints[1] = r;
            }
            //combinations 3 aces
            //$scope.currentPoints[1] = woA + 1 + 1 + 1;
            //$scope.currentPoints[1] = woA + 1 + 1 + 11;
            //$scope.currentPoints[2] = woA + 1 + 11 + 11;
            //$scope.currentPoints[3] = woA + 11 + 11 + 11;

        } else if (acesCount == 4) {
            var woA = $scope.currentPoints[0] - 4;
            var r1 = woA + 1 + 1 + 1 + 1;
            if (r1 < 22) {
                $scope.currentPoints[1] = r1;
                var r2 = woA + 1 + 1 + 1 + 11;
                if (r2 < 22) {
                    $scope.currentPoints[2] = r2;
                }
            }
            //combinations 4 aces
            //$scope.currentPoints[1] = woA + 1 + 1 + 1 + 1;
            //$scope.currentPoints[2] = woA + 1 + 1 + 1 + 11;
            //$scope.currentPoints[3] = woA + 1 + 1 + 11 + 11;
            //$scope.currentPoints[4] = woA + 1 + 11 + 11 + 11;
            //$scope.currentPoints[5] = woA + 11 + 11 + 11 + 11;
        }

        //check if we have a winner
        
        angular.forEach($scope.currentPoints, function (v, k) {
            if (v == 21) {
                $scope.gameMessage = 'You WIN !!!';
                $scope.btnDeal = $scope.btnBreak = true;
                $scope.btnStart = false;
                if (breakNotActive) {
                    $scope.money += 30;
                }
                
            } else if (v > 21 && k > 0) {
                $scope.currentPoints.splice(k, 1);
            }
        });

    }

    //is recursive
    var _randomSelect = function () {

        $scope.btnDeal = true;
        var i = Math.floor(Math.random() * 52) + 1;

        if ((i - 1) in $scope.cards) {

            $scope.selected.push($scope.cards[i - 1]);

            $scope.cards.splice((i - 1), 1);

            if ($scope.cards.length > 0) {
                $scope.btnDeal = $scope.btnBreak = false;
            }

        } else {
            _randomSelect();
        }

        if ($scope.selected.length == 1) {
            $scope.currentTpl = '/tpl.html';
        }
        _refreshScore();
    };

    $scope.start = function () {
        $scope.money -= 10;
        $scope.cards = null;
        $scope.scores = [];
        $scope.selected = [];
        $scope.currentPoints = [];
        $scope.currentTpl = null;
        $scope.gameMessage = '';

        $http.get("/api/deck").success(function (response) {
            $scope.cards = response.cards;
            $scope.btnStart = true;
            $scope.btnDeal = false;
        });
    };

    $scope.deal = function () {
        _randomSelect();
        $scope.renderCards = true;
    };

    $scope.break = function () {
        breakNotActive = false;
        if ($scope.selected.length < 2) {
            $scope.money += 10;
        } else {
            $scope.money += 5;
        }
        while ($scope.gameMessage == '') {
            $scope.deal();
        }
        breakNotActive = true;
    };
});