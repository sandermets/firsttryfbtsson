﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace btssn2.Controllers
{
    /// <summary>
    /// Structure for single card
    /// </summary>
    public class card
    {
        public int nr;
        public string sign;
        public string suit;
        public int[] points;

        public card() { }
    }

    /// <summary>
    /// defining and holding list of cards
    /// </summary>
    public class deck
    {
        public List<card> cards = new List<card>();
        public deck()
        {

            cards.Add(new card { nr=1, sign = "A", suit = "Club", points = new int[] { 1, 11 } });
            cards.Add(new card { nr = 2, sign = "2", suit = "Club", points = new int[] { 2 } });
            cards.Add(new card { nr = 3, sign = "3", suit = "Club", points = new int[] { 3 } });
            cards.Add(new card { nr = 4, sign = "4", suit = "Club", points = new int[] { 4 } });
            cards.Add(new card { nr = 5, sign = "5", suit = "Club", points = new int[] { 5 } });
            cards.Add(new card { nr = 6, sign = "6", suit = "Club", points = new int[] { 6 } });
            cards.Add(new card { nr = 7, sign = "7", suit = "Club", points = new int[] { 7 } });
            cards.Add(new card { nr = 8, sign = "8", suit = "Club", points = new int[] { 8 } });
            cards.Add(new card { nr = 9, sign = "9", suit = "Club", points = new int[] { 9 } });
            cards.Add(new card { nr = 10, sign = "10", suit = "Club", points = new int[] { 10 } });
            cards.Add(new card { nr = 11, sign = "J", suit = "Club", points = new int[] { 10 } });
            cards.Add(new card { nr = 12, sign = "Q", suit = "Club", points = new int[] { 10 } });
            cards.Add(new card { nr = 13, sign = "K", suit = "Club", points = new int[] { 10 } });

            cards.Add(new card { nr = 14, sign = "A", suit = "Spade", points = new int[] { 1, 11 } });
            cards.Add(new card { nr = 15, sign = "2", suit = "Spade", points = new int[] { 2 } });
            cards.Add(new card { nr = 16, sign = "3", suit = "Spade", points = new int[] { 3 } });
            cards.Add(new card { nr = 17, sign = "4", suit = "Spade", points = new int[] { 4 } });
            cards.Add(new card { nr = 18, sign = "5", suit = "Spade", points = new int[] { 5 } });
            cards.Add(new card { nr = 19, sign = "6", suit = "Spade", points = new int[] { 6 } });
            cards.Add(new card { nr = 20, sign = "7", suit = "Spade", points = new int[] { 7 } });
            cards.Add(new card { nr = 21, sign = "8", suit = "Spade", points = new int[] { 8 } });
            cards.Add(new card { nr =22, sign = "9", suit = "Spade", points = new int[] { 9 } });
            cards.Add(new card { nr = 23, sign = "10", suit = "Spade", points = new int[] { 10 } });
            cards.Add(new card { nr = 24, sign = "J", suit = "Spade", points = new int[] { 10 } });
            cards.Add(new card { nr = 25, sign = "Q", suit = "Spade", points = new int[] { 10 } });
            cards.Add(new card { nr = 26, sign = "K", suit = "Spade", points = new int[] { 10 } });

            cards.Add(new card { nr = 27, sign = "A", suit = "Heart", points = new int[] { 1, 11 } });
            cards.Add(new card { nr = 28, sign = "2", suit = "Heart", points = new int[] { 2 } });
            cards.Add(new card { nr = 29, sign = "3", suit = "Heart", points = new int[] { 3 } });
            cards.Add(new card { nr = 30, sign = "4", suit = "Heart", points = new int[] { 4 } });
            cards.Add(new card { nr = 31, sign = "5", suit = "Heart", points = new int[] { 5 } });
            cards.Add(new card { nr = 32, sign = "6", suit = "Heart", points = new int[] { 6 } });
            cards.Add(new card { nr = 33, sign = "7", suit = "Heart", points = new int[] { 7 } });
            cards.Add(new card { nr = 34, sign = "8", suit = "Heart", points = new int[] { 8 } });
            cards.Add(new card { nr = 35, sign = "9", suit = "Heart", points = new int[] { 9 } });
            cards.Add(new card { nr = 36, sign = "10", suit = "Heart", points = new int[] { 10 } });
            cards.Add(new card { nr = 37, sign = "J", suit = "Heart", points = new int[] { 10 } });
            cards.Add(new card { nr = 38, sign = "Q", suit = "Heart", points = new int[] { 10 } });
            cards.Add(new card { nr = 39, sign = "K", suit = "Heart", points = new int[] { 10 } });

            cards.Add(new card { nr = 40, sign = "A", suit = "Diamond", points = new int[] { 1, 11 } });
            cards.Add(new card { nr = 41, sign = "2", suit = "Diamond", points = new int[] { 2 } });
            cards.Add(new card { nr = 42, sign = "3", suit = "Diamond", points = new int[] { 3 } });
            cards.Add(new card { nr = 43, sign = "4", suit = "Diamond", points = new int[] { 4 } });
            cards.Add(new card { nr = 44, sign = "5", suit = "Diamond", points = new int[] { 5 } });
            cards.Add(new card { nr = 45, sign = "6", suit = "Diamond", points = new int[] { 6 } });
            cards.Add(new card { nr = 46, sign = "7", suit = "Diamond", points = new int[] { 7 } });
            cards.Add(new card { nr = 47, sign = "8", suit = "Diamond", points = new int[] { 8 } });
            cards.Add(new card { nr = 48, sign = "9", suit = "Diamond", points = new int[] { 9 } });
            cards.Add(new card { nr = 49, sign = "10", suit = "Diamond", points = new int[] { 10 } });
            cards.Add(new card { nr = 50, sign = "J", suit = "Diamond", points = new int[] { 10 } });
            cards.Add(new card { nr = 51, sign = "Q", suit = "Diamond", points = new int[] { 10 } });
            cards.Add(new card { nr = 52, sign = "K", suit = "Diamond", points = new int[] { 10 } });

            
            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class dataResult
    {
        public List<card> cards;
        public string logic = "if( 1 > 0) alert(3); console.log(this);";
        public dataResult()
        {
            var deck = new deck();
            this.cards = deck.cards;
        }
    }

    public class DeckController : ApiController
    {
        // GET api/deck
        public dataResult Get()
        {
            var d = new dataResult();

            return d;
            //return new string[] { "value1", "value2" };
        }

        // GET api/deck/5
        public card Get(int id)
        {
            if (id < 1 || id > 52)
            {
                return new card();
            }
            var d = new deck();
            return d.cards[(id - 1)];

        }

        /*
        // POST api/deck
        public void Post([FromBody]string value)
        {
        }

        // PUT api/deck/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
        */
    }
}
